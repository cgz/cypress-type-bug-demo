// https://docs.cypress.io/api/table-of-contents

describe('The Test', () => {
  it('Elements', () => {
    cy.visit('/')
    cy.get('demo-component').shadow().find('input[name=email]').should('exist')
        .type('wrong')
    cy.get('demo-component').shadow().find('input[name=password]').should('exist')
        .type('password')
    cy.get('demo-component').shadow().find('input[name=email]').clear()
        .type('example@example.com')
    cy.get('demo-component').shadow().find('button').should('exist')
  })
})
