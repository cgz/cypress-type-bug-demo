# vue-web-comp-css-injection-demo

Simple demo that shows a way to use css in vue-components which are nested in a custom element using vue 3 defineCustomElement.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
