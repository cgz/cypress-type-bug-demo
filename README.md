# cypress-type-bug-demo

This is a minimal example for the "type"-bug in cypress when testing components in shadow dom.


## Steps to reproduce
1. `cd web-components_cypress ; yarn install ; yarn run serve`
2. `cd cypress ; yarn install ; yarn run open`
3. run the test, receive the error: ![](Screenshot.png)
